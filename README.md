# Mobile Cafe

You are a Mobile Engineer at Mobile Cafe Inc, a company that makes smart coffee makers that come to you! The coffee makers are deployed across the country at large, corporate headquarters and tech campuses with multiple stories and sometimes multiple buildings.

Your boss comes to you and presents the following Use Case for which he would like for you to design and implement a prototype mobile app:

A User, let's call them Dana, needs to locate the nearest coffee. The User opens a mobile app and logs in. After logging in, a User would like to use the mobile app to find a coffee maker. The coffee maker presented to the user should meet the following criteria:

- have the shortest line of people waiting for coffee
- have a sufficient coffee supply to fulfill the user's order should they place it
- be the shortest distance from the User


# Exercise Instructions

Please create a branch of this repository. Submit all code, supporting documents, tests, etc in this branch.

Choose any mobile framework you like.

You DO NOT have to create a login screen unless you really, really love login screens and want to. :) You do not need to create nav other than buttons or links needed to fulfill the above use case.

Design and code a small, toy application that lets the User find a smart coffee maker that meets the three criteria described in the Use Case above. Please provide a short description of your solution and instructions on how to demo it in a committed file called DEMO.txt

# Running The Dev Server

We recommend using a python3 virtual env. All the dependencies are listed in requirements.txt and should be installed into the virtual env. After all deps are installed run with `python3 server.py` and the dev server should be running on port 8000 .

# Endpoints

The `/machines` endpoint returns information about the current state of all Mobile Coffee Machines in the building. Here is some example output
```
[
   {
      "floor":"6.0",
      "id":0,
      "order_queue":2,
      "supply_level":0,
      "x":"66.0",
      "y":"71.0"
   },
   {
      "floor":"0.0",
      "id":1,
      "order_queue":0,
      "supply_level":19,
      "x":"49.999996",
      "y":"33.0"
   }
]
```
There can be a variable number of robots.

The `/location` endpoint returns the user's location within the building. See example output below

```
{"floor":0,"x":48,"y":12}
```